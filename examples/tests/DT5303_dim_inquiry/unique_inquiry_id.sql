select count(*) from (
    select
        inquiry_id
    from
        dev_dw_data_team.dim_inquiry
    group by
        1
    having count(*) > 1
)
