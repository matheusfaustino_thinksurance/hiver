# hiver

Run and test models remotely in **dev** or **prod** schemas using the command line.

- **run** a model to populate its table
![Example run](./docs/images/run_example.png)
- **test** a model to assure its quality
![Example test](./docs/images/test_example.png)

Please refer to [this documentation](docs/CONCEPTS.md) for relevant concepts underlying `hiver` implementation and usage.
## Setup
### System dependencies
Windows

- Install C++ compiler (version 14 or above) for your Windows version ([Visual Studio Build Tools page](https://visualstudio.microsoft.com/downloads/))


### Install
Create virtual environment
```bash
python3 -m venv venv
source venv/bin/activate
```

Install from the Bitbucket repository
```bash
pip install git+ssh://git@bitbucket.org/lpillmann/hiver.git
```

## Configuration
1. Initialize the tool
    ```bash
    hiver init
    ```

1. Create a file named `.env` in the folder informed in the previous step (e.g. `/path/to/your_user/.hiver`)

1. Add set the following environment variables according to your credentials and paths
    ```bash
    HIVE_PROJECT_DIR='/path/to/.../tsbigdata'  # where to look for HQL files
    HIVE_AUTH_MECHANISM=PLAIN
    HIVE_HOST=thanos.thinksurance.de
    HIVE_PORT=10000
    HIVE_USER=...

    PRESTO_TESTS_DIR='/.../hiver/examples/tests'  # where to look for SQL files (tests)
    PRESTO_HOST=ancient.thinksurance.de
    PRESTO_PORT=8082
    PRESTO_USER=...
    PRESTO_CATALOG=hive

    HIVER_SCHEMA_MAPPINGS_YAML_FILEPATH='/.../hiver/examples/schema_mappings.yaml'  # where to look for target schema mappings
    ```
## Usage
### Run models
Dev enviroment (dev schemas)
```bash
hiver run --models DC3017_cleaned_reference_industry
```

Prod enviroment (prod schemas)
```bash
hiver run --models DC3017_cleaned_reference_industry --target prod
```

Run multiple models
```bash
hiver run --models \
DC3017_cleaned_reference_industry \
DC3014_cleaned_reference_pool \
5108_dt_flat_brokers
```

Use verbosity option to see the queries executed
```bash
hiver run --models DC3014_cleaned_reference_pool -vvv
```

### Test models
```bash
hiver test --models \
DC3009_cleaned_reference_dealbreak_reason \
DC3063_cleaned_e2e_calculation_dealbreak
```

## Developing

Create virtual environment
```bash
python3 -m venv venv
source venv/bin/activate
```

Install in developing mode
```bash
pip install --editable .
```

Now you are able to change the code and test using the CLI command. Whenever ready to publish, please **create a pull request** to be reviewed by the peers.

## Built with

- [impyla](https://github.com/cloudera/impyla) - Cloudera's DB-API 2.0 client for Impala and Hive (HiveServer2 protocol)
- [presto-python-client](https://github.com/prestodb/presto-python-client) - Official DB-API client for Presto
- [sqlparse](https://github.com/andialbrecht/sqlparse) - For splitting HQL/SQL statements
- [Rich](https://github.com/willmcgugan/rich) - For rich text and formatting in the terminal

## Acknowledgments
This CLI tool is heavily inspired by [dbt](https://www.getdbt.com/product/what-is-dbt/) command line interface.

## Similar tools
- [dbt-core](https://github.com/dbt-labs/dbt-core) - dbt enables data analysts and engineers to transform their data using the same practices that software engineers use to build applications
- [flamy](https://github.com/flaminem/flamy) - a database manager for Apache Hive