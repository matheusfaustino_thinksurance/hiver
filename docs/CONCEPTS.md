# Concepts
This section describes relevant concepts underlying `hiver` implementation and usage.
## Data pipeline
Given a set of source datasets, a **data pipeline** can be defined as the *sequence transformation steps* performed on those datasets to produce artifacts that feed reports, dashboards and other data products. 

![Data Warehouse pipeline](./images/data_warehouse_pipeline.png)

- A common implementation of a data pipeline is to load data into a [data warehouse](https://en.wikipedia.org/wiki/Data_warehouse) and use its own computational power to execute the transformation steps. This approach is often referred to as **ELT** (extract, load and transform)
- It can combine several heterogeneous sources of data
- The transformation layers are defined based on the chosen data modeling techniques


## What is a model?
A **model** is a transformation step in the data pipeline. 

![Example models](./images/models_example.png)

- Generally a model is materialized as a **table** in the data warehouse
- A model consumes data from sources (first layer in the data warehouse) and/or from other models

## Production vs. development environments
Separation of environments is possible by using schemas as **namespaces**. The same table can have multiple versions - one in each schema.

![Data Warehouse environments](./images/data_warehouse_environments.png)

- Test and production pipelines consume from the same data sources
- New code is always ran and tested in the development environment *before* being promoted to production
- By leveraging this concept, anyone performing changes or adding new models to the pipeline can be more productive and be less worried about submitting breaking changes

## What are "tests" in an ELT data pipeline?
A test is the comparison of a query result to an expected result. For example:

- the count of rows with duplicated `id` should be zero - *uniqueness*
- the count of rows with null `id` column should be zero - *not null*
- the latest records in a source table should be within the hour - *freshness*

Each model in the pipeline should be covered with tests. Passing all tests can be used as the criterion to promote new code from development to production environments.

## Why develop locally with a Command Line Interface?
Having a local development setup saves time and is more comfortable - no need to connect and deal with servers directly.

Using the command line is efficient - no need to rely on complex graphical interfaces: just type a few commands and it works. 

Plus, it becomes easier to have automated deployments - the ability to ship releases more frequently without manual steps - once **local development** and **local testing** are adopted.
