from setuptools import setup

setup(
    name="hiver",
    version="0.0.1",
    packages=["hiver"],
    install_requires=[
        "impyla==0.17.0",
        "presto-python-client==0.8.2",
        "python-dotenv==0.19.2",
        "rich==10.16.1",
        "sqlparse==0.4.2",
        "PyYAML==6.0",
    ],
    entry_points={
        "console_scripts": ["hiver=hiver.cli:main"],
    },
)
