import logging
from posixpath import dirname
import socket
import os
import yaml
import sys

from rich.console import Console
from rich.logging import RichHandler

logging.basicConfig(
    level="WARN", format="%(message)s", datefmt="[%X]", handlers=[RichHandler()]
)
log = logging.getLogger(__name__)
log.setLevel("INFO")
console = Console()

REQUIRED_CONFIG_ENV_VARS = [
    "HIVE_PROJECT_DIR",
    "HIVE_AUTH_MECHANISM",
    "HIVE_HOST",
    "HIVE_PORT",
    "HIVE_USER",
    "PRESTO_TESTS_DIR",
    "PRESTO_HOST",
    "PRESTO_PORT",
    "PRESTO_USER",
    "PRESTO_CATALOG",
    "HIVER_SCHEMA_MAPPINGS_YAML_FILEPATH",
]


def read_file(filepath):
    with open(filepath, "r") as f:
        contents = f.read()
    return "\n" + contents


def parse_yaml(filepath):
    with open(filepath, "r") as f:
        parsed = yaml.safe_load(f)
    return parsed


def get_hostname():
    return socket.gethostname()


def get_config_dir_path():
    config_dir_name = ".hiver"
    return os.path.join(os.path.expanduser("~"), config_dir_name)


def create_config_dir(path=None):
    if path:
        raise NotImplementedError(
            f"Custom configuration directory path not supported yet."
        )
    if not path:
        path = get_config_dir_path()
    if os.path.exists(path):
        log.info(f"Directory {path} already exists. Skipping initialization.")
        sys.exit(1)
    os.mkdir(path)
    console.print(
        f"Created directory {path} \nPlease add your [bold].env[/bold] file there :sunglasses:"
    )
    sys.exit(1)
