import functools
import glob
import logging
import os
import time
import sys
from collections import namedtuple

import sqlparse
from impala.dbapi import connect as hive_connect
from prestodb.dbapi import connect as presto_connect
from prestodb.exceptions import PrestoExternalError, PrestoUserError
from rich.console import Console
from rich.logging import RichHandler
from rich.syntax import Syntax

from hiver.helpers import parse_yaml, read_file


PROJECT_DIR = os.getenv("HIVE_PROJECT_DIR")
TESTS_DIR = os.getenv("PRESTO_TESTS_DIR")
SCHEMA_MAPPINGS_YAML_FILEPATH = os.getenv("HIVER_SCHEMA_MAPPINGS_YAML_FILEPATH")

logging.basicConfig(
    level="WARN", format="%(message)s", datefmt="[%X]", handlers=[RichHandler()]
)
log = logging.getLogger(__name__)
log.setLevel("INFO")
console = Console()

Test = namedtuple("Test", "name query")


class Model:
    """
    A model represents the data transformations needed to populate a table in Hive. It also contains
    the data tests to be performed against its Hive table using Presto.

    It hosts raw statements/queries read from the files as well as the "compiled" ones ready to
    be executed.
    """

    def __init__(self, name):
        self.name = name
        self.raw_statements = []
        self.compiled = {"target": None, "statements": [], "tests": []}

    @property
    def filepath(self):
        """Return the complete path of the file within the project directory"""
        search_pattern = f"{PROJECT_DIR}/**/{self.name}"
        if ".hql" not in self.name:
            search_pattern += ".hql"
        paths = glob.glob(search_pattern, recursive=True)
        if len(paths) == 0:
            log.error(
                f"No HQL file paths found for filename {self.name} in directory {PROJECT_DIR}"
            )
            sys.exit(1)
        if len(paths) > 1:
            log.error(f"More than one path found for filename {self.name}: {paths}")
            sys.exit(1)
        return paths[0]

    @property
    def raw_query(self):
        return read_file(self.filepath)

    @property
    def tests_directory_path(self):
        """Return the complete path of the directory containing tests for the model"""
        search_pattern = f"{TESTS_DIR}/**/{self.name}"
        paths = glob.glob(search_pattern, recursive=True)
        if len(paths) == 0:
            log.error(
                f"No test paths found for model {self.name} in directory {TESTS_DIR}"
            )
            sys.exit(1)
        if len(paths) > 1:
            log.error(f"More than one test path found for model {self.name}: {paths}")
            sys.exit(1)
        return paths[0]

    @property
    def raw_tests(self):
        tests = []
        for filename in os.listdir(self.tests_directory_path):
            query = read_file(os.path.join(self.tests_directory_path, filename))
            tests.append(Test(filename.replace(".sql", ""), query))
        return tests


class Compiler:
    """The entity responsible for preparing the HQL/SQL statements to be executed."""

    def __init__(self, target):
        self.target = target or "dev"

    def __call__(self, model):
        return self.compile(model)

    @property
    @functools.lru_cache()
    def schema_mappings(self):
        return parse_yaml(SCHEMA_MAPPINGS_YAML_FILEPATH)

    def _replace_schemas(self, statements, target):
        """Return a new set of statements with schemas replaced according to the target's schema mapping"""
        if target not in self.schema_mappings.keys():
            log.error(
                f"Target not found in available schema mappings. Available targets: {['dev', *self.schema_mappings.keys()]}"
            )
            sys.exit(1)
        schema_mappings = self.schema_mappings[target]
        replaced_statements = []
        for statement in statements:
            for original, new in schema_mappings.items():
                if original in statement:
                    statement = statement.replace(f" {original}.", f" {new}.")
                    console.log(f"Replaced schema {original} for {new}")
            replaced_statements.append(statement)
        return replaced_statements

    @staticmethod
    def _sanitize_hql(hql):
        return hql.replace(";", "")

    def _parse(self, model):
        """Parse file contents into executable HQL statements"""
        model.raw_statements = [
            self._sanitize_hql(statement)
            for statement in sqlparse.split(model.raw_query)
        ]
        return model

    def compile(self, model):
        """Prepare the statements to be executed according to the chosen target"""
        model = self._parse(model)
        model.compiled["target"] = self.target
        if self.target == "dev":
            model.compiled["statements"] = model.raw_statements.copy()
            console.log(
                f"Schema replacement skipped for model [bold]{model.name}[/bold] (target {self.target})"
            )
            return model
        compiled_statements = self._replace_schemas(model.raw_statements, self.target)
        if self.target == "prod":
            assert all(["dev_" not in statement for statement in compiled_statements])
        model.compiled["statements"] = compiled_statements
        return model

    def compile_tests(self, model):
        """Prepare the test statements to be executed according to the chosen target"""
        model.compiled["target"] = self.target
        if self.target == "dev":
            model.compiled["tests"] = model.raw_tests.copy()
            console.log(
                f"Schema replacement skipped for model [bold]{model.name}[/bold] (target {self.target})"
            )
            return model
        for test in model.raw_tests:
            name = test.name
            raw_query = test.query
            compiled_query = self._replace_schemas([raw_query], self.target)
            model.compiled["tests"].append(Test(name, compiled_query[0]))
        if self.target == "prod":
            assert all(
                ["dev_" not in statement for statement in model.compiled["tests"]]
            )
        return model


class Executor:
    """The entity responsible for executing the HQL statements against Hive to populate tables."""

    def __init__(self, verbosity):
        self.verbosity = verbosity

    def __call__(self, models):
        return self.execute(models)

    @property
    def conn(self):
        return hive_connect(
            auth_mechanism=os.getenv("HIVE_AUTH_MECHANISM", "PLAIN"),
            host=os.getenv("HIVE_HOST"),
            port=os.getenv("HIVE_PORT"),
            user=os.getenv("HIVE_USER"),
        )

    def execute(self, models):
        cursor = self.conn.cursor()
        for model in models:
            statements_to_execute = model.compiled["statements"].copy()
            with console.status(
                f"Executing model [bold]{model.name}[/bold] in [bold magenta]{model.compiled['target']}[/bold magenta] ({len(statements_to_execute)} statements)",
                spinner="simpleDotsScrolling",
            ):
                count = 0
                total = len(statements_to_execute)

                console.log(f"[bold]{model.name}[/bold] started")
                while statements_to_execute:
                    loop_start = time.time()
                    statement = statements_to_execute.pop(0)
                    if self.verbosity >= 3:
                        syntax = Syntax(statement, "sql")
                        console.print(syntax)
                    cursor.execute(statement)
                    console.log(
                        f"statement {count + 1} of {total} completed ({time.time() - loop_start:.1f} s)"
                    )
                    count += 1

                console.log(f"[bold]{model.name}[/bold] completed")


class TestsExecutor(Executor):
    """The entity responsible for executing the SQL statements against Presto to perform data tests."""

    @property
    def conn(self):
        return presto_connect(
            host=os.getenv("PRESTO_HOST"),
            port=os.getenv("PRESTO_PORT"),
            user=os.getenv("PRESTO_USER"),
            catalog=os.getenv("PRESTO_CATALOG"),
        )

    @property
    def hive_conn(self):
        return hive_connect(
            auth_mechanism=os.getenv("HIVE_AUTH_MECHANISM", "PLAIN"),
            host=os.getenv("HIVE_HOST"),
            port=os.getenv("HIVE_PORT"),
            user=os.getenv("HIVE_USER"),
        )

    def execute(self, models):
        cursor = self.conn.cursor()
        test_results = dict(passed=list(), failed=list())
        for model in models:
            tests_to_execute = model.compiled["tests"].copy()
            with console.status(
                f"Executing tests for model [bold]{model.name}[/bold] in [bold magenta]{model.compiled['target']}[/bold magenta] ({len(tests_to_execute)} tests)",
                spinner="simpleDotsScrolling",
            ):
                count = 0
                total = len(tests_to_execute)
                console.log(f"[bold]{model.name}[/bold] started")
                while tests_to_execute:
                    loop_start = time.time()
                    test = tests_to_execute.pop(0)
                    query_to_execute = (
                        test.query + "\n limit 1"
                    )  # Enforce returning single row
                    if self.verbosity >= 3:
                        syntax = Syntax(query_to_execute, "sql")
                        console.print(syntax)
                    cursor.execute(query_to_execute)

                    is_executed_with_hive = False
                    try:
                        result = cursor.fetchone()[0]

                    # This is done to enable us to write Hive queries for integration tests.
                    # If it fails due to syntax with Presto, it will try to run with Hive
                    except PrestoUserError as e:
                        if e.error_name == "SYNTAX_ERROR":
                            is_executed_with_hive = True
                            cursor.close()
                            hive_cursor = self.hive_conn.cursor()
                            hive_cursor.execute(query_to_execute)
                            fetched = hive_cursor.fetchone()[0]
                            # Hive cursor can return None when there are no rows
                            result = 0
                            if fetched:
                                result = fetched
                            hive_cursor.close()

                    # Presto connector doesn't work for transactional partitioned tables.
                    # In those cases, use Hive connector to perform the tests instead.
                    except PrestoExternalError as e:
                        if e.error_name == "HIVE_INVALID_BUCKET_FILES":
                            is_executed_with_hive = True
                            cursor.close()
                            hive_cursor = self.hive_conn.cursor()
                            hive_cursor.execute(query_to_execute)
                            fetched = hive_cursor.fetchone()[0]
                            # Hive cursor can return None when there are no rows
                            result = 0
                            if fetched:
                                result = fetched
                            hive_cursor.close()
                        else:
                            raise e

                    expected_result = 0  # All tests should return 0 if passed
                    additional_info_msg = ""
                    if is_executed_with_hive:
                        additional_info_msg = "- [italic]executed with Hive[/italic]"
                    try:
                        assert result == expected_result
                        console.log(
                            f"test [bold]{test.name}[/bold] {count + 1} of {total} [green]passed[/green] ({time.time() - loop_start:.1f} s) {additional_info_msg}"
                        )
                        test_results["passed"].append(f"{model.name}.{test.name}")
                    except AssertionError:
                        console.log(
                            f"test [bold]{test.name}[/bold] {count + 1} of {total} [red]failed[/red] ({time.time() - loop_start:.1f} s) - Expected {expected_result} got {result} {additional_info_msg}"
                        )
                        test_results["failed"].append(f"{model.name}.{test.name}")
                    count += 1

                console.log(f"[bold]{model.name}[/bold] completed")

        passed, failed = len(test_results['passed']), len(test_results['failed'])
        console.log(
            f"\nFinished running tests: TOTAL {passed + failed} PASSED {passed} FAILED {failed}"
        )
        if failed > 0:
            raise RuntimeError(f"One or more tests failed: {test_results['failed']}")
