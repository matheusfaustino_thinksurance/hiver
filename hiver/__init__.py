import os
from dotenv import load_dotenv

from hiver.helpers import get_config_dir_path

load_dotenv(os.path.join(get_config_dir_path(), ".env"))
