import argparse
import logging
import os
import sys

from rich.console import Console
from rich.logging import RichHandler

from hiver.core import Model, Compiler, Executor, TestsExecutor
from hiver.helpers import (
    REQUIRED_CONFIG_ENV_VARS,
    create_config_dir,
    get_config_dir_path,
    get_hostname,
)

PRODUCTION_SERVER_HOSTNAME = os.getenv(
    "THINKSURANCE_DATATEAM_PRODUCTION_SERVER_HOSTNAME", "vision.thinksurance.de"
)


logging.basicConfig(
    level="WARN", format="%(message)s", datefmt="[%X]", handlers=[RichHandler()]
)
log = logging.getLogger(__name__)
log.setLevel("INFO")
console = Console()


def init(args):
    create_config_dir(args.path)


def run(args):
    models = [Model(model) for model in args.models]
    console.log(f"Models to run: {[model.name for model in models]}")
    compiler = Compiler(args.target)
    compiled_models = [compiler(model) for model in models]
    executor = Executor(args.verbosity)
    executor(compiled_models)


def test(args):
    models = [Model(model) for model in args.models]
    console.log(f"Models to test: {[model.name for model in models]}")
    compiler = Compiler(args.target)
    compiled_models = [compiler.compile_tests(model) for model in models]
    executor = TestsExecutor(args.verbosity)
    executor(compiled_models)


def main():
    parser = argparse.ArgumentParser(
        description="hiver tool - Run and test models remotely in dev or prod schemas using the command line."
    )
    subparsers = parser.add_subparsers()

    parser_init = subparsers.add_parser("init")
    parser_init.add_argument(
        "-p", "--path", default=None, help="custom configuration directory path"
    )
    parser_init.set_defaults(func=init)

    parser_run = subparsers.add_parser("run")
    parser_run.add_argument("-m", "--models", help="one or more model names", nargs="+")
    parser_run.add_argument("-t", "--target", default=None, help="target (dev or prod)")
    parser_run.add_argument(
        "-v",
        "--verbosity",
        action="count",
        default=0,
        help="level of verbosity",
    )
    parser_run.set_defaults(func=run)

    parser_test = subparsers.add_parser("test")
    parser_test.add_argument(
        "-m", "--models", help="one or more model names", nargs="+"
    )
    parser_test.add_argument(
        "-t", "--target", default=None, help="target (dev or prod)"
    )
    parser_test.add_argument(
        "-v",
        "--verbosity",
        action="count",
        default=0,
        help="level of verbosity",
    )
    parser_test.set_defaults(func=test)

    args = parser.parse_args()

    if args.func == run:
        if args.target == "prod" and get_hostname() != PRODUCTION_SERVER_HOSTNAME:
            log.error(f"Using target `prod` is only allowed in production server.")
            sys.exit(1)

    if args.func == run or args.func == test:
        missing = []
        for required_config in REQUIRED_CONFIG_ENV_VARS:
            if not os.getenv(required_config):
                missing.append(required_config)
        if missing:
            log.error(
                "Configuration error. Please make sure to set all required environment variables in "
                + f"the `.env` file in {get_config_dir_path()}. Missing environment variables: {missing}"
            )
            sys.exit(1)

    args.func(args)


if __name__ == "__main__":
    main()
